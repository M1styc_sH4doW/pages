/**
 * This is for testing
 */

let text = document.getElementById("inputUbicacion");
let send = document.getElementById("btn");

send.addEventListener("click", () => {
  var ubicacion = text.value;
  if (ubicacion.length > 3) {
    var xhr = new XMLHttpRequest();
    xhr.open(
      "GET",
      "https://nominatim.openstreetmap.org/search?format=json&q=" + ubicacion,
      true
    );
    xhr.onload = function () {
      if (this.status == 200) {
        var data = JSON.parse(this.responseText);
        if (data.length > 0) {
          var resultado = data[0];
          var coordenadas = resultado.lat + ", " + resultado.lon;
          var pais = resultado.display_name.split(",").pop();
          var zona = resultado.display_name;
          // Aquí puedes usar las variables coordenadas, pais y zona para mostrar la información al usuario
          console.log(coordenadas, pais, zona);
        }
      } else {
        console.error("El sitio es " + ubicacion);
      }
    };
    xhr.send();
  } else {
    console.error("Ubicación demasiado corta");
  }
});
