DROP TABLE IF EXISTS likings;
CREATE TABLE likings (
 user_id INTEGER PRIMARY KEY AUTOINCREMENT,
 age_range_min BLOB NOT NULL DEFAULT 25,
 age_range_max BLOB NOT NULL DEFAULT 30,
 smoking BLOB DEFAULT NULL,
 pets BLOB DEFAULT NULL,
 films BLOB DEFAULT NULL,
 fitness BLOB DEFAULT NULL,
 nightlife BLOB DEFAULT NULL,
 travel BLOB DEFAULT NULL,
 sociability BLOB DEFAULT NULL,
 work_position BLOB DEFAULT NULL,
 location BLOB DEFAULT NULL,
 personality BLOB DEFAULT NULL,
 zodiac BLOB DEFAULT NULL,
 music BLOB DEFAULT NULL,
 education BLOB DEFAULT NULL,
 reading BLOB DEFAULT NULL,
 coding BLOB DEFAULT NULL,
 videogames BLOB DEFAULT NULL,
 sport BLOB DEFAULT NULL, FOREIGN KEY (user_id) REFERENCES users(id)
);
insert into likings (
  user_id, age_range_min, age_range_max, 
  smoking, pets, films, fitness, nightlife, 
  travel, sociability, work_position, 
  location, personality, zodiac, music, 
  education, reading, coding, videogames, 
  sport
) 
values 
  (
    1, 99, 28, true, 'Cat', 'Comedy', 'NULL', 
    'NULL', 'NULL', 'Introvert', 'Account Representative III', 
    'Suburb', 'Friendly', 'Aquarius', 
    'Jazz', 'High School', 'Romance', 
    'Java', 'NULL', 'Basketball'
  );
insert into likings (
  user_id, age_range_min, age_range_max, 
  smoking, pets, films, fitness, nightlife, 
  travel, sociability, work_position, 
  location, personality, zodiac, music, 
  education, reading, coding, videogames, 
  sport
) 
values 
  (
    2, 66, 37, false, 'NULL', 'Action', 'Yoga', 
    'Clubs', 'Mountains', 'NULL', 'Editor', 
    'City', 'Analytical', 'Libra', 'Country', 
    'High School', 'Non-Fiction', 'JavaScript', 
    'NULL', 'Soccer'
  );
insert into likings (
  user_id, age_range_min, age_range_max, 
  smoking, pets, films, fitness, nightlife, 
  travel, sociability, work_position, 
  location, personality, zodiac, music, 
  education, reading, coding, videogames, 
  sport
) 
values 
  (
    3, 60, 30, true, 'NULL', 'NULL', 'Yoga', 
    'NULL', 'NULL', 'Extrovert', 'Software Engineer I', 
    'Rural', 'Adventurous', 'Virgo', 
    'Classical', 'Graduate School', 
    'Fiction', 'NULL', 'Action', 'Basketball'
  );
insert into likings (
  user_id, age_range_min, age_range_max, 
  smoking, pets, films, fitness, nightlife, 
  travel, sociability, work_position, 
  location, personality, zodiac, music, 
  education, reading, coding, videogames, 
  sport
) 
values 
  (
    4, 66, 61, true, 'NULL', 'NULL', 'NULL', 
    'Bars', 'City', 'Introvert', 'Computer Systems Analyst III', 
    'Suburb', 'Creative', 'Aquarius', 
    'Country', 'High School', 'Romance', 
    'NULL', 'NULL', 'Basketball'
  );
insert into likings (
  user_id, age_range_min, age_range_max, 
  smoking, pets, films, fitness, nightlife, 
  travel, sociability, work_position, 
  location, personality, zodiac, music, 
  education, reading, coding, videogames, 
  sport
) 
values 
  (
    5, 32, 75, false, 'NULL', 'Action', 'NULL', 
    'NULL', 'NULL', 'Extrovert', 'Director of Sales', 
    'Suburb', 'NULL', 'Pisces', 'NULL', 
    'NULL', 'NULL', 'C++', 'RPG', 'Soccer'
  );
insert into likings (
  user_id, age_range_min, age_range_max, 
  smoking, pets, films, fitness, nightlife, 
  travel, sociability, work_position, 
  location, personality, zodiac, music, 
  education, reading, coding, videogames, 
  sport
) 
values 
  (
    6, 47, 97, false, 'Bird', 'NULL', 'Gym', 
    'Parties', 'Countryside', 'Introvert', 
    'Senior Cost Accountant', 'Rural', 
    'Friendly', 'Leo', 'Rock', 'College', 
    'Non-Fiction', 'Python', 'RPG', 'Football'
  );
insert into likings (
  user_id, age_range_min, age_range_max, 
  smoking, pets, films, fitness, nightlife, 
  travel, sociability, work_position, 
  location, personality, zodiac, music, 
  education, reading, coding, videogames, 
  sport
) 
values 
  (
    7, 84, 48, false, 'Fish', 'NULL', 'Yoga', 
    'NULL', 'Mountains', 'Introvert', 
    'Occupational Therapist', 'NULL', 
    'NULL', 'Scorpio', 'Rock', 'NULL', 
    'Romance', 'C++', 'Strategy', 'Football'
  );
insert into likings (
  user_id, age_range_min, age_range_max, 
  smoking, pets, films, fitness, nightlife, 
  travel, sociability, work_position, 
  location, personality, zodiac, music, 
  education, reading, coding, videogames, 
  sport
) 
values 
  (
    8, 92, 33, true, 'Cat', 'Action', 'Running', 
    'Clubs', 'NULL', 'NULL', 'Health Coach II', 
    'Suburb', 'Friendly', 'Capricorn', 
    'Pop', 'Graduate School', 'Non-Fiction', 
    'C++', 'Adventure', 'Soccer'
  );
insert into likings (
  user_id, age_range_min, age_range_max, 
  smoking, pets, films, fitness, nightlife, 
  travel, sociability, work_position, 
  location, personality, zodiac, music, 
  education, reading, coding, videogames, 
  sport
) 
values 
  (
    9, 25, 29, false, 'Dog', 'Action', 'NULL', 
    'Bars', 'Beach', 'NULL', 'Senior Cost Accountant', 
    'Suburb', 'NULL', 'Scorpio', 'Rock', 
    'NULL', 'Fiction', 'C++', 'NULL', 
    'Soccer'
  );
insert into likings (
  user_id, age_range_min, age_range_max, 
  smoking, pets, films, fitness, nightlife, 
  travel, sociability, work_position, 
  location, personality, zodiac, music, 
  education, reading, coding, videogames, 
  sport
) 
values 
  (
    10, 27, 71, true, 'NULL', 'Drama', 'Yoga', 
    'Parties', 'Beach', 'Extrovert', 
    'Environmental Tech', 'Suburb', 
    'Analytical', 'Aquarius', 'Pop', 
    'Graduate School', 'Non-Fiction', 
    'C++', 'Action', 'Basketball'
  );
insert into likings (
  user_id, age_range_min, age_range_max, 
  smoking, pets, films, fitness, nightlife, 
  travel, sociability, work_position, 
  location, personality, zodiac, music, 
  education, reading, coding, videogames, 
  sport
) 
values 
  (
    11, 98, 45, false, 'NULL', 'Drama', 'NULL', 
    'Parties', 'City', 'NULL', 'Information Systems Manager', 
    'Rural', 'Adventurous', 'Capricorn', 
    'Hip Hop', 'NULL', 'Mystery', 'Java', 
    'Action', 'Soccer'
  );
insert into likings (
  user_id, age_range_min, age_range_max, 
  smoking, pets, films, fitness, nightlife, 
  travel, sociability, work_position, 
  location, personality, zodiac, music, 
  education, reading, coding, videogames, 
  sport
) 
values 
  (
    12, 28, 37, true, 'Dog', 'Romance', 'NULL', 
    'Bars', 'City', 'Introvert', 'Project Manager', 
    'City', 'Creative', 'Aries', 'NULL', 
    'Graduate School', 'NULL', 'C++', 
    'NULL', 'Tennis'
  );
insert into likings (
  user_id, age_range_min, age_range_max, 
  smoking, pets, films, fitness, nightlife, 
  travel, sociability, work_position, 
  location, personality, zodiac, music, 
  education, reading, coding, videogames, 
  sport
) 
values 
  (
    13, 72, 51, false, 'Bird', 'Romance', 
    'Running', 'Bars', 'City', 'Extrovert', 
    'Compensation Analyst', 'Suburb', 
    'Creative', 'Pisces', 'Jazz', 'NULL', 
    'Mystery', 'Python', 'Action', 'Tennis'
  );
insert into likings (
  user_id, age_range_min, age_range_max, 
  smoking, pets, films, fitness, nightlife, 
  travel, sociability, work_position, 
  location, personality, zodiac, music, 
  education, reading, coding, videogames, 
  sport
) 
values 
  (
    14, 73, 72, true, 'Fish', 'NULL', 'Running', 
    'Bars', 'City', 'Introvert', 'Desktop Support Technician', 
    'Rural', 'Adventurous', 'Aries', 
    'NULL', 'College', 'Romance', 'Ruby', 
    'Strategy', 'Football'
  );
insert into likings (
  user_id, age_range_min, age_range_max, 
  smoking, pets, films, fitness, nightlife, 
  travel, sociability, work_position, 
  location, personality, zodiac, music, 
  education, reading, coding, videogames, 
  sport
) 
values 
  (
    15, 60, 40, true, 'Bird', 'Comedy', 'NULL', 
    'Clubs', 'Beach', 'Introvert', 'Marketing Manager', 
    'NULL', 'Analytical', 'Cancer', 'Country', 
    'NULL', 'NULL', 'NULL', 'RPG', 'Baseball'
  );
insert into likings (
  user_id, age_range_min, age_range_max, 
  smoking, pets, films, fitness, nightlife, 
  travel, sociability, work_position, 
  location, personality, zodiac, music, 
  education, reading, coding, videogames, 
  sport
) 
values 
  (
    16, 100, 85, false, 'Fish', 'Comedy', 
    'Swimming', 'Clubs', 'NULL', 'NULL', 
    'VP Marketing', 'City', 'Adventurous', 
    'Libra', 'Hip Hop', 'College', 'Science Fiction', 
    'C++', 'Action', 'Basketball'
  );
insert into likings (
  user_id, age_range_min, age_range_max, 
  smoking, pets, films, fitness, nightlife, 
  travel, sociability, work_position, 
  location, personality, zodiac, music, 
  education, reading, coding, videogames, 
  sport
) 
values 
  (
    17, 90, 23, true, 'Cat', 'Drama', 'Running', 
    'Parties', 'Countryside', 'NULL', 
    'Administrative Assistant II', 
    'Rural', 'Adventurous', 'Scorpio', 
    'Rock', 'College', 'Non-Fiction', 
    'Python', 'RPG', 'Baseball'
  );
insert into likings (
  user_id, age_range_min, age_range_max, 
  smoking, pets, films, fitness, nightlife, 
  travel, sociability, work_position, 
  location, personality, zodiac, music, 
  education, reading, coding, videogames, 
  sport
) 
values 
  (
    18, 88, 70, false, 'Dog', 'Romance', 
    'Swimming', 'NULL', 'Mountains', 
    'NULL', 'Developer I', 'City', 'Creative', 
    'Virgo', 'Jazz', 'High School', 'Romance', 
    'JavaScript', 'NULL', 'NULL'
  );
insert into likings (
  user_id, age_range_min, age_range_max, 
  smoking, pets, films, fitness, nightlife, 
  travel, sociability, work_position, 
  location, personality, zodiac, music, 
  education, reading, coding, videogames, 
  sport
) 
values 
  (
    19, 97, 41, false, 'NULL', 'Horror', 
    'Running', 'Clubs', 'Mountains', 
    'Extrovert', 'Senior Developer', 
    'Suburb', 'NULL', 'Cancer', 'Pop', 
    'High School', 'NULL', 'JavaScript', 
    'NULL', 'Basketball'
  );
insert into likings (
  user_id, age_range_min, age_range_max, 
  smoking, pets, films, fitness, nightlife, 
  travel, sociability, work_position, 
  location, personality, zodiac, music, 
  education, reading, coding, videogames, 
  sport
) 
values 
  (
    20, 25, 62, true, 'Cat', 'Action', 'Swimming', 
    'NULL', 'City', 'Extrovert', 'Assistant Manager', 
    'NULL', 'Adventurous', 'NULL', 'NULL', 
    'High School', 'Science Fiction', 
    'C++', 'Sports', 'NULL'
  );
insert into likings (
  user_id, age_range_min, age_range_max, 
  smoking, pets, films, fitness, nightlife, 
  travel, sociability, work_position, 
  location, personality, zodiac, music, 
  education, reading, coding, videogames, 
  sport
) 
values 
  (
    21, 61, 19, false, 'NULL', 'NULL', 'Running', 
    'Clubs', 'Mountains', 'Extrovert', 
    'Software Test Engineer II', 'Suburb', 
    'Creative', 'Aquarius', 'Classical', 
    'NULL', 'Mystery', 'Ruby', 'Action', 
    'Soccer'
  );
insert into likings (
  user_id, age_range_min, age_range_max, 
  smoking, pets, films, fitness, nightlife, 
  travel, sociability, work_position, 
  location, personality, zodiac, music, 
  education, reading, coding, videogames, 
  sport
) 
values 
  (
    22, 96, 36, true, 'Fish', 'Drama', 'NULL', 
    'Parties', 'Countryside', 'Extrovert', 
    'Nurse', 'City', 'Creative', 'Cancer', 
    'Jazz', 'NULL', 'Fiction', 'Python', 
    'Action', 'Tennis'
  );
insert into likings (
  user_id, age_range_min, age_range_max, 
  smoking, pets, films, fitness, nightlife, 
  travel, sociability, work_position, 
  location, personality, zodiac, music, 
  education, reading, coding, videogames, 
  sport
) 
values 
  (
    23, 84, 27, false, 'Fish', 'Comedy', 
    'Yoga', 'Parties', 'City', 'NULL', 
    'Chemical Engineer', 'City', 'NULL', 
    'Gemini', 'Rock', 'Graduate School', 
    'Mystery', 'Ruby', 'Action', 'Football'
  );
insert into likings (
  user_id, age_range_min, age_range_max, 
  smoking, pets, films, fitness, nightlife, 
  travel, sociability, work_position, 
  location, personality, zodiac, music, 
  education, reading, coding, videogames, 
  sport
) 
values 
  (
    24, 29, 90, true, 'Fish', 'Drama', 'Running', 
    'Bars', 'Mountains', 'NULL', 'Programmer I', 
    'NULL', 'Friendly', 'Scorpio', 'Classical', 
    'High School', 'Science Fiction', 
    'Python', 'RPG', 'Basketball'
  );
insert into likings (
  user_id, age_range_min, age_range_max, 
  smoking, pets, films, fitness, nightlife, 
  travel, sociability, work_position, 
  location, personality, zodiac, music, 
  education, reading, coding, videogames, 
  sport
) 
values 
  (
    25, 79, 92, false, 'Dog', 'Romance', 
    'Yoga', 'Clubs', 'City', 'Introvert', 
    'Help Desk Technician', 'Rural', 
    'NULL', 'Gemini', 'Classical', 'College', 
    'Non-Fiction', 'Ruby', 'Sports', 
    'Football'
  );
insert into likings (
  user_id, age_range_min, age_range_max, 
  smoking, pets, films, fitness, nightlife, 
  travel, sociability, work_position, 
  location, personality, zodiac, music, 
  education, reading, coding, videogames, 
  sport
) 
values 
  (
    26, 39, 33, true, 'Fish', 'NULL', 'Yoga', 
    'Clubs', 'Beach', 'Extrovert', 'Environmental Tech', 
    'NULL', 'Creative', 'Sagittarius', 
    'Classical', 'College', 'Mystery', 
    'Ruby', 'NULL', 'Soccer'
  );
insert into likings (
  user_id, age_range_min, age_range_max, 
  smoking, pets, films, fitness, nightlife, 
  travel, sociability, work_position, 
  location, personality, zodiac, music, 
  education, reading, coding, videogames, 
  sport
) 
values 
  (
    27, 89, 84, false, 'Fish', 'Drama', 'NULL', 
    'Clubs', 'Mountains', 'Extrovert', 
    'Developer III', 'NULL', 'Adventurous', 
    'Scorpio', 'Jazz', 'NULL', 'NULL', 
    'JavaScript', 'Action', 'Football'
  );
insert into likings (
  user_id, age_range_min, age_range_max, 
  smoking, pets, films, fitness, nightlife, 
  travel, sociability, work_position, 
  location, personality, zodiac, music, 
  education, reading, coding, videogames, 
  sport
) 
values 
  (
    28, 70, 80, false, 'Fish', 'Drama', 'Yoga', 
    'Parties', 'NULL', 'NULL', 'Paralegal', 
    'Suburb', 'Analytical', 'Sagittarius', 
    'Hip Hop', 'Graduate School', 'Mystery', 
    'Ruby', 'Strategy', 'Football'
  );
insert into likings (
  user_id, age_range_min, age_range_max, 
  smoking, pets, films, fitness, nightlife, 
  travel, sociability, work_position, 
  location, personality, zodiac, music, 
  education, reading, coding, videogames, 
  sport
) 
values 
  (
    29, 34, 22, true, 'Dog', 'NULL', 'Gym', 
    'Clubs', 'Countryside', 'Extrovert', 
    'Account Representative III', 'NULL', 
    'Adventurous', 'Aries', 'Classical', 
    'NULL', 'Science Fiction', 'JavaScript', 
    'Action', 'NULL'
  );
insert into likings (
  user_id, age_range_min, age_range_max, 
  smoking, pets, films, fitness, nightlife, 
  travel, sociability, work_position, 
  location, personality, zodiac, music, 
  education, reading, coding, videogames, 
  sport
) 
values 
  (
    30, 52, 83, true, 'Fish', 'Romance', 
    'Swimming', 'Bars', 'Mountains', 
    'Introvert', 'Electrical Engineer', 
    'Rural', 'Creative', 'NULL', 'Country', 
    'Graduate School', 'Non-Fiction', 
    'JavaScript', 'Sports', 'Football'
  );
insert into likings (
  user_id, age_range_min, age_range_max, 
  smoking, pets, films, fitness, nightlife, 
  travel, sociability, work_position, 
  location, personality, zodiac, music, 
  education, reading, coding, videogames, 
  sport
) 
values 
  (
    31, 72, 19, false, 'Cat', 'Drama', 'Gym', 
    'NULL', 'NULL', 'NULL', 'Assistant Media Planner', 
    'NULL', 'Analytical', 'Libra', 'NULL', 
    'College', 'Mystery', 'Ruby', 'Strategy', 
    'Basketball'
  );
insert into likings (
  user_id, age_range_min, age_range_max, 
  smoking, pets, films, fitness, nightlife, 
  travel, sociability, work_position, 
  location, personality, zodiac, music, 
  education, reading, coding, videogames, 
  sport
) 
values 
  (
    32, 89, 42, false, 'NULL', 'Action', 
    'Running', 'Clubs', 'Mountains', 
    'NULL', 'Safety Technician III', 
    'City', 'Adventurous', 'Sagittarius', 
    'Classical', 'College', 'Romance', 
    'C++', 'RPG', 'Soccer'
  );
insert into likings (
  user_id, age_range_min, age_range_max, 
  smoking, pets, films, fitness, nightlife, 
  travel, sociability, work_position, 
  location, personality, zodiac, music, 
  education, reading, coding, videogames, 
  sport
) 
values 
  (
    33, 19, 40, true, 'Fish', 'NULL', 'Running', 
    'Bars', 'Countryside', 'Extrovert', 
    'Geologist II', 'City', 'Analytical', 
    'Libra', 'NULL', 'NULL', 'Science Fiction', 
    'Java', 'RPG', 'Football'
  );
insert into likings (
  user_id, age_range_min, age_range_max, 
  smoking, pets, films, fitness, nightlife, 
  travel, sociability, work_position, 
  location, personality, zodiac, music, 
  education, reading, coding, videogames, 
  sport
) 
values 
  (
    34, 89, 73, true, 'Cat', 'Romance', 'Gym', 
    'Clubs', 'NULL', 'Extrovert', 'Senior Quality Engineer', 
    'Suburb', 'Analytical', 'Sagittarius', 
    'Classical', 'High School', 'Non-Fiction', 
    'Java', 'Strategy', 'NULL'
  );
insert into likings (
  user_id, age_range_min, age_range_max, 
  smoking, pets, films, fitness, nightlife, 
  travel, sociability, work_position, 
  location, personality, zodiac, music, 
  education, reading, coding, videogames, 
  sport
) 
values 
  (
    35, 29, 45, false, 'Fish', 'Action', 
    'Gym', 'NULL', 'Beach', 'Introvert', 
    'VP Quality Control', 'NULL', 'Creative', 
    'Gemini', 'Hip Hop', 'Graduate School', 
    'Science Fiction', 'NULL', 'Sports', 
    'NULL'
  );
insert into likings (
  user_id, age_range_min, age_range_max, 
  smoking, pets, films, fitness, nightlife, 
  travel, sociability, work_position, 
  location, personality, zodiac, music, 
  education, reading, coding, videogames, 
  sport
) 
values 
  (
    36, 27, 80, true, 'Bird', 'Romance', 
    'Yoga', 'Clubs', 'City', 'Extrovert', 
    'Research Nurse', 'City', 'Analytical', 
    'Scorpio', 'Hip Hop', 'Graduate School', 
    'NULL', 'Python', 'RPG', 'Football'
  );
insert into likings (
  user_id, age_range_min, age_range_max, 
  smoking, pets, films, fitness, nightlife, 
  travel, sociability, work_position, 
  location, personality, zodiac, music, 
  education, reading, coding, videogames, 
  sport
) 
values 
  (
    37, 56, 26, false, 'Dog', 'Romance', 
    'NULL', 'Bars', 'City', 'Extrovert', 
    'Structural Engineer', 'Suburb', 
    'Analytical', 'Virgo', 'Classical', 
    'College', 'Fiction', 'Python', 'RPG', 
    'Tennis'
  );
insert into likings (
  user_id, age_range_min, age_range_max, 
  smoking, pets, films, fitness, nightlife, 
  travel, sociability, work_position, 
  location, personality, zodiac, music, 
  education, reading, coding, videogames, 
  sport
) 
values 
  (
    38, 26, 34, false, 'Cat', 'Romance', 
    'Yoga', 'Parties', 'Countryside', 
    'Extrovert', 'Director of Sales', 
    'Rural', 'NULL', 'Scorpio', 'NULL', 
    'NULL', 'Fiction', 'Python', 'RPG', 
    'Soccer'
  );
insert into likings (
  user_id, age_range_min, age_range_max, 
  smoking, pets, films, fitness, nightlife, 
  travel, sociability, work_position, 
  location, personality, zodiac, music, 
  education, reading, coding, videogames, 
  sport
) 
values 
  (
    39, 39, 35, true, 'Dog', 'Drama', 'Yoga', 
    'NULL', 'Beach', 'Extrovert', 'VP Quality Control', 
    'Suburb', 'Creative', 'Aries', 'Jazz', 
    'Graduate School', 'Mystery', 'Java', 
    'NULL', 'Soccer'
  );
insert into likings (
  user_id, age_range_min, age_range_max, 
  smoking, pets, films, fitness, nightlife, 
  travel, sociability, work_position, 
  location, personality, zodiac, music, 
  education, reading, coding, videogames, 
  sport
) 
values 
  (
    40, 18, 70, false, 'Fish', 'Action', 
    'Swimming', 'Clubs', 'Countryside', 
    'NULL', 'Technical Writer', 'City', 
    'Friendly', 'Aries', 'NULL', 'College', 
    'NULL', 'NULL', 'Sports', 'Baseball'
  );
