DROP TABLE IF EXISTS users;
CREATE TABLE users (
  id INTEGER PRIMARY KEY AUTOINCREMENT, 
  creation_date BLOB NOT NULL, name BLOB NOT NULL DEFAULT 'name', 
  surname BLOB NOT NULL DEFAULT 'surmame', 
  age BLOB NOT NULL DEFAULT 0, password BLOB UNIQUE NOT NULL DEFAULT 'pas', 
  gender BLOB NOT NULL DEFAULT 'gender', 
  orientation BLOB NOT NULL DEFAULT 'orientation', 
  relation BLOB NOT NULL DEFAULT 'rel', 
  description BLOB NOT NULL DEFAULT 'description'
);

insert into users (
  id, creation_date, name, surname, age, 
  password, gender, orientation, relation, 
  description
) 
values 
  (
    1, '12/26/1923', 'Clarinda', 'Takis', 
    45, 'xY7~i%>joV|k', 'Female', 'orientation3', 
    'relation8', 'Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.'
  );
insert into users (
  id, creation_date, name, surname, age, 
  password, gender, orientation, relation, 
  description
) 
values 
  (
    2, '2/21/1909', 'Thorndike', 'Novkovic', 
    87, 'wG0.ltMR{V5', 'Male', 'orientation7', 
    'relation5', 'Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.'
  );
insert into users (
  id, creation_date, name, surname, age, 
  password, gender, orientation, relation, 
  description
) 
values 
  (
    3, '12/17/2078', 'Ceil', 'Prudham', 
    98, 'lK2?HPKzr<A~OGV', 'Female', 
    'orientation2', 'relation3', 'Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.

Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.'
  );
insert into users (
  id, creation_date, name, surname, age, 
  password, gender, orientation, relation, 
  description
) 
values 
  (
    4, '3/21/1928', 'Raymond', 'Rookesby', 
    97, 'eI0~&iR~j@Jk', 'Male', 'orientation4', 
    'relation10', 'Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.

Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.

Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.'
  );
insert into users (
  id, creation_date, name, surname, age, 
  password, gender, orientation, relation, 
  description
) 
values 
  (
    5, '8/28/1963', 'Margarette', 'Kincey', 
    33, 'iY3@#\CGohwry\#z', 'Female', 
    'orientation2', 'relation8', 'Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.

Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.'
  );
insert into users (
  id, creation_date, name, surname, age, 
  password, gender, orientation, relation, 
  description
) 
values 
  (
    6, '10/27/1919', 'Tova', 'Mara', 82, 
    'tX4(#ED\`#IuEX', 'Female', 'orientation5', 
    'relation10', 'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.

Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.'
  );
insert into users (
  id, creation_date, name, surname, age, 
  password, gender, orientation, relation, 
  description
) 
values 
  (
    7, '1/22/1957', 'Royce', 'Phillipson', 
    84, 'gB0=jS{jxf2F@', 'Male', 'orientation7', 
    'relation8', 'Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.

In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.'
  );
insert into users (
  id, creation_date, name, surname, age, 
  password, gender, orientation, relation, 
  description
) 
values 
  (
    8, '3/4/2069', 'Nicholle', 'Sandeford', 
    75, 'pG4$g?VNq45EM', 'Female', 'orientation3', 
    'relation9', 'In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.

Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.

Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.'
  );
insert into users (
  id, creation_date, name, surname, age, 
  password, gender, orientation, relation, 
  description
) 
values 
  (
    9, '2/14/1927', 'Giusto', 'Sherwin', 
    32, 'lN4_L(@iUR%4E7o', 'Male', 'orientation3', 
    'relation5', 'Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.'
  );
insert into users (
  id, creation_date, name, surname, age, 
  password, gender, orientation, relation, 
  description
) 
values 
  (
    10, '10/22/1903', 'Artemis', 'Doulton', 
    66, 'vC4{2"I79kS6z>eo', 'Male', 'orientation4', 
    'relation3', 'Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.

Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.

Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.'
  );
insert into users (
  id, creation_date, name, surname, age, 
  password, gender, orientation, relation, 
  description
) 
values 
  (
    11, '5/23/2097', 'Cherri', 'Mallender', 
    20, 'lQ9\tSR+nB0Vm_@', 'Female', 
    'orientation7', 'relation3', 'Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.

Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.'
  );
insert into users (
  id, creation_date, name, surname, age, 
  password, gender, orientation, relation, 
  description
) 
values 
  (
    12, '10/6/1926', 'Winifield', 'Lyfield', 
    85, 'uD8/R''%e', 'Male', 'orientation1', 
    'relation9', 'In congue. Etiam justo. Etiam pretium iaculis justo.'
  );
insert into users (
  id, creation_date, name, surname, age, 
  password, gender, orientation, relation, 
  description
) 
values 
  (
    13, '7/23/2008', 'Grazia', 'Slavin', 
    26, 'oP2<q{l13_a(@e$', 'Female', 
    'orientation1', 'relation1', 'In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.'
  );
insert into users (
  id, creation_date, name, surname, age, 
  password, gender, orientation, relation, 
  description
) 
values 
  (
    14, '6/7/1988', 'Denney', 'Cinnamond', 
    91, 'lH8!f.?,b#6VS/,f', 'Male', 'orientation1', 
    'relation6', 'Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam.'
  );
insert into users (
  id, creation_date, name, surname, age, 
  password, gender, orientation, relation, 
  description
) 
values 
  (
    15, '7/26/2046', 'Ruby', 'Bartoletti', 
    95, 'hU1/!pu9=0g&QqX', 'Male', 'orientation2', 
    'relation8', 'Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.'
  );
insert into users (
  id, creation_date, name, surname, age, 
  password, gender, orientation, relation, 
  description
) 
values 
  (
    16, '8/13/2056', 'Dolores', 'Ilem', 
    28, 'zB9|''tPLi', 'Female', 'orientation1', 
    'relation10', 'Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.'
  );
insert into users (
  id, creation_date, name, surname, age, 
  password, gender, orientation, relation, 
  description
) 
values 
  (
    17, '3/14/2018', 'Natalina', 'Ridolfo', 
    70, 'oM3<O4"YoPn', 'Female', 'orientation5', 
    'relation9', 'Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.'
  );
insert into users (
  id, creation_date, name, surname, age, 
  password, gender, orientation, relation, 
  description
) 
values 
  (
    18, '3/22/2099', 'Angelita', 'Slym', 
    74, 'dS2@Su4/', 'Female', 'orientation3', 
    'relation5', 'Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.'
  );
insert into users (
  id, creation_date, name, surname, age, 
  password, gender, orientation, relation, 
  description
) 
values 
  (
    19, '8/31/1984', 'Charlotte', 'Simpson', 
    37, 'fK4<03zz=K', 'Female', 'orientation4', 
    'relation2', 'Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam.'
  );
insert into users (
  id, creation_date, name, surname, age, 
  password, gender, orientation, relation, 
  description
) 
values 
  (
    20, '9/27/2060', 'Garnet', 'Touret', 
    69, 'fP2+Iv7e1dT', 'Female', 'orientation7', 
    'relation6', 'Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.

Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.

Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.'
  );
insert into users (
  id, creation_date, name, surname, age, 
  password, gender, orientation, relation, 
  description
) 
values 
  (
    21, '3/26/1995', 'Burton', 'Penkethman', 
    35, 'sA2`\\M$IU_lDQ', 'Male', 'orientation2', 
    'relation8', 'Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.'
  );
insert into users (
  id, creation_date, name, surname, age, 
  password, gender, orientation, relation, 
  description
) 
values 
  (
    22, '11/27/1945', 'Elaina', 'Sebborn', 
    19, 'iY8!7+bl''E1gU', 'Female', 'orientation4', 
    'relation4', 'Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.'
  );
insert into users (
  id, creation_date, name, surname, age, 
  password, gender, orientation, relation, 
  description
) 
values 
  (
    23, '11/29/2020', 'Randie', 'Kynson', 
    78, 'lX8#II_z''/B}7LP_', 'Female', 
    'orientation6', 'relation4', 'Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.

Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.

Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.'
  );
insert into users (
  id, creation_date, name, surname, age, 
  password, gender, orientation, relation, 
  description
) 
values 
  (
    24, '6/14/2095', 'Garrik', 'Edwicke', 
    89, 'jB1>uZ{2$%@{i$', 'Male', 'orientation6', 
    'relation6', 'Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.

Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.

Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.'
  );
insert into users (
  id, creation_date, name, surname, age, 
  password, gender, orientation, relation, 
  description
) 
values 
  (
    25, '3/21/1979', 'Gloriane', 'Eddleston', 
    57, 'mD8\%}8rsW', 'Female', 'orientation4', 
    'relation6', 'Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.

Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.'
  );
insert into users (
  id, creation_date, name, surname, age, 
  password, gender, orientation, relation, 
  description
) 
values 
  (
    26, '9/14/1964', 'Kameko', 'Warbys', 
    43, 'fB0.S''Q7XjONl1)u', 'Female', 
    'orientation1', 'relation8', 'Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.'
  );
insert into users (
  id, creation_date, name, surname, age, 
  password, gender, orientation, relation, 
  description
) 
values 
  (
    27, '11/3/1903', 'Dahlia', 'Strowlger', 
    61, 'vI2)?fa3Y~J/0\c', 'Agender', 
    'orientation7', 'relation6', 'Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.

Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.'
  );
insert into users (
  id, creation_date, name, surname, age, 
  password, gender, orientation, relation, 
  description
) 
values 
  (
    28, '9/27/2073', 'Krystyna', 'Yurchenko', 
    80, 'tT1$''0*@DOJ', 'Female', 'orientation4', 
    'relation5', 'Sed ante. Vivamus tortor. Duis mattis egestas metus.

Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.'
  );
insert into users (
  id, creation_date, name, surname, age, 
  password, gender, orientation, relation, 
  description
) 
values 
  (
    29, '11/3/1957', 'Tyrone', 'Perigo', 
    45, 'gI7/4NHHr2Fc_9%', 'Male', 'orientation6', 
    'relation4', 'In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.

Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.'
  );
insert into users (
  id, creation_date, name, surname, age, 
  password, gender, orientation, relation, 
  description
) 
values 
  (
    30, '1/10/2087', 'Mathew', 'Titcomb', 
    53, 'lG5{&!"{', 'Male', 'orientation7', 
    'relation5', 'In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.'
  );
insert into users (
  id, creation_date, name, surname, age, 
  password, gender, orientation, relation, 
  description
) 
values 
  (
    31, '1/28/2032', 'Sidnee', 'McMenamy', 
    88, 'kA6}fY{$)}J7vh%', 'Male', 'orientation5', 
    'relation2', 'Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.'
  );
insert into users (
  id, creation_date, name, surname, age, 
  password, gender, orientation, relation, 
  description
) 
values 
  (
    32, '5/15/1931', 'Tine', 'Endrici', 
    93, 'qO9<yvw{!x(eZ<&', 'Female', 
    'orientation5', 'relation5', 'Fusce consequat. Nulla nisl. Nunc nisl.

Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.'
  );
insert into users (
  id, creation_date, name, surname, age, 
  password, gender, orientation, relation, 
  description
) 
values 
  (
    33, '12/20/1918', 'Lay', 'Prickett', 
    83, 'tJ1"czoS', 'Male', 'orientation1', 
    'relation7', 'Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.'
  );
insert into users (
  id, creation_date, name, surname, age, 
  password, gender, orientation, relation, 
  description
) 
values 
  (
    34, '5/29/1946', 'Moise', 'Anfosso', 
    61, 'iN6},rjD_J*G', 'Male', 'orientation6', 
    'relation10', 'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.

Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.'
  );
insert into users (
  id, creation_date, name, surname, age, 
  password, gender, orientation, relation, 
  description
) 
values 
  (
    35, '10/3/1940', 'Mathe', 'Crosthwaite', 
    66, 'rQ6#o.6v', 'Non-binary', 'orientation4', 
    'relation9', 'Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.

Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.

Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.'
  );
insert into users (
  id, creation_date, name, surname, age, 
  password, gender, orientation, relation, 
  description
) 
values 
  (
    36, '3/17/2075', 'Marya', 'Gauvain', 
    76, 'kK0}x6''6`', 'Female', 'orientation5', 
    'relation1', 'In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.'
  );
insert into users (
  id, creation_date, name, surname, age, 
  password, gender, orientation, relation, 
  description
) 
values 
  (
    37, '7/21/2062', 'Lauralee', 'Brotherwood', 
    37, 'vV5!Kz9''|+', 'Female', 'orientation4', 
    'relation8', 'Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.

Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.

Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam.'
  );
insert into users (
  id, creation_date, name, surname, age, 
  password, gender, orientation, relation, 
  description
) 
values 
  (
    38, '5/19/1916', 'Madel', 'St. Clair', 
    91, 'vI7$50q3h?4|', 'Female', 'orientation7', 
    'relation3', 'Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.'
  );
insert into users (
  id, creation_date, name, surname, age, 
  password, gender, orientation, relation, 
  description
) 
values 
  (
    39, '3/29/1975', 'Phyllis', 'Stroband', 
    71, 'vM6&N2Ed)5#v%@ZS', 'Female', 
    'orientation2', 'relation1', 'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.'
  );
insert into users (
  id, creation_date, name, surname, age, 
  password, gender, orientation, relation, 
  description
) 
values 
  (
    40, '9/17/2027', 'Kerrin', 'Georgeson', 
    98, 'rE9@VaLvr1nJ#UW9', 'Female', 
    'orientation7', 'relation5', 'Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.'
  );
