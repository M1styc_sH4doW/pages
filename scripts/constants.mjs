/* Constants Module
    Here will be the contants varible of the Sinder app
    that will be used several times arround the application 
    and the big, but one-usage, functions that changing them
    from here will change the entire funcionality of the app 
*/

export let version = "Beta 1.0";

/**
 *
 * @param {db} db First SQLite query to make all the main databases
 */
export async function createBasicData(db) {
  try {
    db.exec(`-- Create a table named 'users'                    
    -- This table will have the basic information of the profile
    DROP TABLE IF EXISTS users;
    CREATE TABLE users (
     id INTEGER PRIMARY KEY AUTOINCREMENT,
     creation_date BLOB NOT NULL,
     name BLOB NOT NULL DEFAULT 'name',
     surname BLOB NOT NULL DEFAULT 'surmame',
     age BLOB NOT NULL DEFAULT 0,
     password BLOB UNIQUE NOT NULL DEFAULT 'pas',
     gender BLOB NOT NULL DEFAULT 'gender',
     orientation BLOB NOT NULL DEFAULT 'orientation',
     relation BLOB NOT NULL DEFAULT 'rel',
     description BLOB NOT NULL DEFAULT 'description'
    );
    -- Now create the second table where extra information that will be given to the algorith
    DROP TABLE IF EXISTS likings;
    CREATE TABLE likings (
     user_id INTEGER PRIMARY KEY AUTOINCREMENT,
     age_range_min BLOB NOT NULL DEFAULT 25,
     age_range_max BLOB NOT NULL DEFAULT 30,
     smoking BLOB DEFAULT NULL,
     pets BLOB DEFAULT NULL,
     films BLOB DEFAULT NULL,
     fitness BLOB DEFAULT NULL,
     nightlife BLOB DEFAULT NULL,
     travel BLOB DEFAULT NULL,
     sociability BLOB DEFAULT NULL,
     work_position BLOB DEFAULT NULL,
     location BLOB DEFAULT NULL,
     personality BLOB DEFAULT NULL,
     zodiac BLOB DEFAULT NULL,
     music BLOB DEFAULT NULL,
     education BLOB DEFAULT NULL,
     reading BLOB DEFAULT NULL,
     coding BLOB DEFAULT NULL,
     videogames BLOB DEFAULT NULL,
     sport BLOB DEFAULT NULL, FOREIGN KEY (user_id) REFERENCES users(id)
    );
    -- I need to create statistics so i need a table for that
    DROP TABLE IF EXISTS statistics;
    CREATE TABLE statistics (
     user_id INTEGER PRIMARY KEY AUTOINCREMENT,
     session_time BLOB,
     yes_counter BLOB DEFAULT 0,
     no_counter BLOB DEFAULT 0,
     last_yes BLOB DEFAULT NULL,
     value_user BLOB NOT NULL DEFAULT 75,
     historic_people_passed BLOB DEFAULT NULL,
     affinity_porcentaje BLOB DEFAULT 0, FOREIGN KEY (user_id) REFERENCES users(id)
    );
    -- For better encryption, one table will have the data of the last iv
    -- This will encrease the randomness on the keys to encrypt
    DROP TABLE IF EXISTS iv_session;
    CREATE TABLE iv_session (
     user_id INTEGER PRIMARY KEY AUTOINCREMENT,
     iv_name BLOB DEFAULT NULL,
     iv_surname BLOB DEFAULT NULL,
     iv_age BLOB DEFAULT NULL,
     iv_password BLOB DEFAULT NULL,
     iv_gender BLOB DEFAULT NULL,
     iv_orientation BLOB DEFAULT NULL,
     iv_relation BLOB DEFAULT NULL,
     iv_description BLOB DEFAULT NULL,
     iv_age_range_min BLOB DEFAULT NULL,
     iv_age_range_max BLOB DEFAULT NULL,
     iv_smoking BLOB DEFAULT NULL,
     iv_pets BLOB DEFAULT NULL,
     iv_films BLOB DEFAULT NULL,
     iv_fitness BLOB DEFAULT NULL,
     iv_nightlife BLOB DEFAULT NULL,
     iv_travel BLOB DEFAULT NULL,
     iv_sociability BLOB DEFAULT NULL,
     iv_work_position BLOB DEFAULT NULL,
     iv_location BLOB DEFAULT NULL,
     iv_personality BLOB DEFAULT NULL,
     iv_zodiac BLOB DEFAULT NULL,
     iv_music BLOB DEFAULT NULL,
     iv_education BLOB DEFAULT NULL,
     iv_reading BLOB DEFAULT NULL,
     iv_coding BLOB DEFAULT NULL,
     iv_videogames BLOB DEFAULT NULL,
     iv_sport BLOB DEFAULT NULL, FOREIGN KEY (user_id) REFERENCES users(id)
    );
    -- For the login, for privacy reasons I need to create a middle table where to store the SHA512 
    -- version of the data to check if someone is logged and then, continue to the main database
    -- only in case the database is cyphered, otherwise is unnecesary due to the fact that everything
    -- is plain text. I know i don't need all the variables but for now, i'll keep them all.
    DROP TABLE IF EXISTS sha_data;
    CREATE TABLE sha_data (
      user_id INTEGER PRIMARY KEY autoincrement,
      sha_name TEXT DEFAULT NULL,
      sha_surname TEXT DEFAULT NULL,
      sha_age TEXT DEFAULT NULL,
      sha_password TEXT DEFAULT NULL,
      sha_gender TEXT DEFAULT NULL,
      sha_orientation TEXT DEFAULT NULL,
      sha_relation TEXT DEFAULT NULL,
      sha_description TEXT DEFAULT NULL,
      sha_age_range_min TEXT DEFAULT NULL,
      sha_age_range_max TEXT DEFAULT NULL,
      sha_smoking TEXT DEFAULT NULL,
      sha_pets TEXT DEFAULT NULL,
      sha_films TEXT DEFAULT NULL,
      sha_fitness TEXT DEFAULT NULL,
      sha_nightlife TEXT DEFAULT NULL,
      sha_travel TEXT DEFAULT NULL,
      sha_sociability TEXT DEFAULT NULL,
      sha_work_position TEXT DEFAULT NULL,
      sha_location TEXT DEFAULT NULL,
      sha_personality TEXT DEFAULT NULL,
      sha_zodiac TEXT DEFAULT NULL,
      sha_music TEXT DEFAULT NULL,
      sha_education TEXT DEFAULT NULL,
      sha_reading TEXT DEFAULT NULL,
      sha_coding TEXT DEFAULT NULL,
      sha_videogames TEXT DEFAULT NULL,
      sha_sport TEXT DEFAULT NULL, FOREIGN KEY (user_id) REFERENCES users(id)
    );`);
  } catch (error) {
    throw error;
  }
}

/**
 * This vector will have the positions of the HTML elements
 * If other part needs to check whether to take info from one element or another
 * this will index the element in question.
 */
export let orderOfElements = [
  "name",
  "surname",
  "age",
  "password",
  "gender",
  "orientation",
  "relation",
  "description",
  "age_range_min",
  "age_range_max",
  "smoking",
  "pets",
  "films",
  "fitness",
  "nightlife",
  "travel",
  "sociability",
  "work_position",
  "location",
  "personality",
  "zodiac",
  "music",
  "education",
  "reading",
  "coding",
  "videogames",
  "sport",
];

/**
 *
 * @returns key to export created with the AES-GCM algorithm
 */
export async function createKey() {
  await window.crypto.subtle
    .generateKey(
      {
        name: "AES-GCM",
        length: 256,
      },
      true, // extractable
      ["encrypt", "decrypt"]
    )
    .then(async function (key) {
      await window.crypto.subtle.exportKey("jwk", key).then((res) => {
        console.log("Exporting Key");
        let blob = new Blob([JSON.stringify(res)], {
          type: "application/json",
        });
        let url = window.URL.createObjectURL(blob);
        let a = document.createElement("a");
        a.href = url;
        a.download = "CryptograficKey.json";
        a.onclick = function () {
          setTimeout(() => window.URL.revokeObjectURL(a.href), 1500);
        };
        a.click();
      });
    });
}

/**
 *
 * @param {message} message to be encrypted
 * @param {key} key to encrypt the message
 */
export async function encryptMessage(message, key) {
  let enc = new TextEncoder();
  let encoded = enc.encode(message);
  // iv will be needed for decryption
  // The iv must never be reused with a given key.
  let iv = window.crypto.getRandomValues(new Uint8Array(16));
  let ciphertext = await window.crypto.subtle.encrypt(
    { name: "AES-GCM", iv: iv },
    key,
    encoded
  );
  return [iv, ciphertext];
}

/**
 *
 * @param {encMessage} The message to be decrypted and the iv, with the form [iv, ciphertext]
 * @param {key} key to use for decryption.
 * @returns {string} Decripted message.
 */
export async function decryptMessage(encMessage, key) {
  // encMessage will be the form [iv, ciphertext]
  let decrypted = await window.crypto.subtle.decrypt(
    { name: "AES-GCM", iv: encMessage[0] },
    key,
    encMessage[1]
  );
  let dec = new TextDecoder();
  return dec.decode(decrypted); // Decripted string
}

/**
 *
 * @param {jwk} JSON Web Key to import in Stringfy version.
 * @returns {CryptoKey} CryptoKey object ready to use as-is in javascript
 */
export async function importKey(jwk) {
  return await window.crypto.subtle.importKey(
    "jwk",
    jwk,
    { name: "AES-GCM" },
    true,
    ["encrypt", "decrypt"]
  );
}

/**
 *
 * @param {string} message Message to have the SHA-384
 * @returns hashHEX string;
 */
export async function digestMessage(message) {
  const msgUint8 = new TextEncoder().encode(message); // encode as (utf-8) Uint8Array
  const hashBuffer = await crypto.subtle.digest("SHA-384", msgUint8); // hash the message
  const hashArray = Array.from(new Uint8Array(hashBuffer)); // convert buffer to byte array
  const hashHex = hashArray
    .map((b) => b.toString(16).padStart(2, "0"))
    .join(""); // convert bytes to hex string
  return hashHex;
}

export async function loadUserData(userData, ivData) {
  // loading screen
  console.log("Loading user data");
  if (userData.length != ivData.length) {
    alert("Error en los datos");
    return;
  } else {
    for (let index in userData) {
    }
  }
}

export async function insertIntoUsers(db, ...values) {
  try {
    db.exec({
      sql: `INSERT INTO users (
      creation_date,
      name,
      surname,
      age,
      PASSWORD,
      gender,
      orientation,
      relation,
      description
     ) VALUES (DATE('now'),
     ?, ?, ?, ?,
     ?, ?, ?, ?
     )`,
      bind: [
        values[0], //name
        values[1], // surname
        values[2], // age
        values[3], // password
        values[4], // gender
        values[5], // orientation
        values[6], // relation
        values[7], // description
      ],
    });
  } catch (error) {
    throw error;
  }
}

export async function insertIntoLikings(db, ...values) {
  try {
    db.exec({
      sql: `INSERT INTO likings (
        age_range_min,
        age_range_max,
        smoking,
        pets,
        films,
        fitness,
        nightlife,
        travel,
        sociability,
        work_position,
        location,
        personality,
        zodiac,
        music,
        education,
        reading,
        coding,
        videogames,
        sport
       ) VALUES(
        ?, ?, ?,
        ?, ?, ?,
        ?, ?, ?,
        ?, ?, ?,
        ?, ?, ?,
        ?, ?, ?, ?
       )`,
      bind: [
        values[0],
        values[1],
        values[2], // smoking
        values[3],
        values[4],
        values[5], // fitness
        values[6],
        values[7],
        values[8], // sociability
        values[9],
        values[10],
        values[11], // personality
        values[12],
        values[13],
        values[14], // educaction
        values[15],
        values[16],
        values[17], //videogames
        values[18],
      ],
    });
  } catch (error) {
    throw error;
  }
}

export async function insertIntoIV(db, ...values) {
  try {
    db.exec({
      sql: `insert into iv_session (
      iv_name,
      iv_surname,
      iv_age,
      iv_password,
      iv_gender,
      iv_orientation,
      iv_relation,
      iv_description,
      iv_age_range_min,
      iv_age_range_max,
      iv_smoking,
      iv_pets,
      iv_films,
      iv_fitness,
      iv_nightlife,
      iv_travel,
      iv_sociability,
      iv_work_position,
      iv_location,
      iv_personality,
      iv_zodiac,
      iv_music,
      iv_education,
      iv_reading,
      iv_coding,
      iv_videogames,
      iv_sport
  ) values (
    ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, -- 13
    ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, -- 26?
    ?
  )`,
      bind: [
        values[0], //name
        values[1], // surname
        values[2], // age
        values[3], // password
        values[4], // gender
        values[5], // orientation
        values[6], // relation
        values[7], // description
        values[8],
        values[9],
        values[10], // smoking
        values[11],
        values[12],
        values[13], // fitness
        values[14],
        values[15],
        values[16], // sociability
        values[17],
        values[18],
        values[19], // personality
        values[20],
        values[21],
        values[22], // educaction
        values[23],
        values[24],
        values[25], //videogames
        values[26],
      ],
    });
  } catch (error) {
    throw error;
  }
}

export async function insertIntoSHAData(db, ...values) {
  try {
    db.exec({
      sql: `insert into sha_data (
        sha_name,
        sha_surname,
        sha_age,
        sha_password,
        sha_gender,
        sha_orientation,
        sha_relation,
        sha_description,
        sha_age_range_min,
        sha_age_range_max,
        sha_smoking,
        sha_pets,
        sha_films,
        sha_fitness,
        sha_nightlife,
        sha_travel,
        sha_sociability,
        sha_work_position,
        sha_location,
        sha_personality,
        sha_zodiac,
        sha_music,
        sha_education,
        sha_reading,
        sha_coding,
        sha_videogames,
        sha_sport  
  ) values (
    ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, -- 13
    ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, -- 26?
    ?
  )`,
      bind: [
        values[0], //name
        values[1], // surname
        values[2], // age
        values[3], // password
        values[4], // gender
        values[5], // orientation
        values[6], // relation
        values[7], // description
        values[8],
        values[9],
        values[10], // smoking
        values[11],
        values[12],
        values[13], // fitness
        values[14],
        values[15],
        values[16], // sociability
        values[17],
        values[18],
        values[19], // personality
        values[20],
        values[21],
        values[22], // educaction
        values[23],
        values[24],
        values[25], //videogames
        values[26],
      ],
    });
  } catch (error) {
    throw error;
  }
}
