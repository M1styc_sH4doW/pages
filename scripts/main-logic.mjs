/**
 * This is the sinder main page logic that goes in the background
 */

// First thing is to get the sqlite ready and put the main
// loaded database in the middle of everything
"use strict";

import { digestMessage, importKey } from "./constants.mjs";

const msgSender = "Main thread:";

// Declaring new worker
let worker = new Worker(new URL("./worker.mjs", import.meta.url), {
  type: "module",
});

// Constants
const form = document.forms["login"];
const uploadedDB = document.getElementById("insert-db");
const keyReception = document.getElementById("insert-key");

/**
 * Important notes, I should consider always to pass to the worker
 * a JSON object because it will make easier to migrate or to send
 * data from place to another.
 */
if (window.Worker) {
  uploadedDB.value = null; // Clean File input
  keyReception.value = null; // Clean File input
  window.addEventListener("load", () => {
    worker.postMessage({ method: "init", message: null });
  });

  uploadedDB.addEventListener("change", () => {
    const file = uploadedDB.files[0];
    worker.postMessage({ method: "loadDB", message: file });
  });

  // The key must be in the worker module
  keyReception.addEventListener("change", async () => {
    let input = keyReception.files[0];
    await new Response(input)
      .json()
      .then((json) => importKey(json))
      .then((res) => worker.postMessage({ method: "loadKey", message: res }))
      .catch(function (error) {
        console.error(`${msgSender} ${error}`);
        alert(
          "There were an error trying to impor the key. Please try again or reload the page"
        );
      });
  });

  form.addEventListener("submit", (event) => {
    event.preventDefault();
    const formData = new FormData(form);
    // Conitnuar aqui
    worker.postMessage({ method: "login", message: formData });
  });

  worker.onmessage = (event) => {
    //console.log("Receiver from worker");
    const method = event.data.method;
    const message = event.data.message;

    switch (method) {
      case "iniciated":
        console.log(`${msgSender} ${message}`);
        break;

      case "readed":
        console.log(`${msgSender} ${message}`);
        break;

      default:
        console.warn(`${msgSender} No method of worker recived`);
        break;
    }
  };
} else {
  alert("Web workers NOT supported. This will not work");
  console.error(`${msgSender} Your browser doesn't support web workers.`);
}
