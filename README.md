<h1>Sinder Open Source</h1>

<h2><strong>Important note</strong>:</h2>
This web application is still <strong>WORK IN PROGRESS</strong> and it does not work but I uploaded it to check how responsive is it.
<hr>

Welcome to the open source web application that mimics the idea of a social application to meet new people, such as Tinder or Bumble, but it does locally in your computer.

## Background

This application is, by itself, a inner joke between friends that I ended up proposing myself to finish like a challenge. Right now it's just meant to be more like an execise or excuse to learning about web development rather than an official application.

# Idea of the proyect

The main goal is to replicate the functionality of social apps like Tinder where people, after creating a profile and set some desired parameters, can found people affine to them. But because I cannot stand the privacy policies in applications of big companies and I don't have my own server, I only can aim to have a local application.

The application is created with the idea of being as simple as possible but with good functionalities and visuals. To do so, all the pages and functionalities are written in JavaScript, HTML, CSS using Fomantic UI Framework and SQlite WASM  as database.

## Usage

This is a web app, meaning that all the process is done locally in the computer without petitions to external server.

- First you begin in `Index.html` which is the Welcome Page of Sinder and the you could access or a page where you can create a user throughout a form or going to the main page.
- In this first option, you will be ask to create a new profile which is going to be created using SQLite WASM compilator from JavaScript to C++ code in the backend.
- In the second option page, after uploading the necessary items to the page, it will recreate the functionalities of a social application.

## License

The license of the proyect is [GNU AFFERO GENERAL PUBLIC LICENSE v3.0 or Later](https://www.gnu.org/licenses/agpl-3.0.html).